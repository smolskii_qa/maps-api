# First AutoTest

Here is an example of Maps API automation tests. It is my first attempt, so enjoy it and try not to cry because of my
skills.
My education process is still carrying on, so I am confident new commits will bring better code.
Here I wrote a simple test, which includes Post, Get, Put and Delete methods based on the MapsAPI by
[Rahul Shetty Academy](https://rahulshettyacademy.com). That API simulates real maps API. In my test, I use it to
create/edit/delete map's places, to get data about them and of course to test it.

## Artifacts

After running this test, you will get some artifacts about it.

### Allure Reports

Check them in ['Gitlab's Pages'](https://smolskii_qa.gitlab.io/maps-api). The report will be created after running
the pipeline, using Docker and GitLab-Runner throw my PC.

Also, the test generates a similar Allure Report on the local machine every time you run it.

### Logs

Check them in [.\logs](https://gitlab.com/smolskii_qa/maps-api/-/tree/master/logs) directory. The logs will be created
after running the code on any machine, using Pytest.

## How to use

There are few ways:

1) Run it manually on your machine
2) Run it by Gitlab's CI/CD engine (available only for the owner now)

## Manually

1. Clone the repo.

2. Install required libraries:
   This set autotests requires some [libs](https://gitlab.com/smolskii_qa/maps-api/-/blob/master/requirements.txt). So,
   if you don't have them, install manually or use pip:

   ```bash
   pip install -r requirements.txt
   ```

3. To just execute this autotest example, run next command from project path:

   ```bash
   python -m pytest -s -v
   ```

   *s and v flags are nor required, but I would recommend to use them to get more useful pytest reports.


4. To run with collecting allure results:

   ```bash
   python -m pytest -s -v --alluredir=allure-results/
   ```

5. To create allure report:

   ```bash
   allure serve allure-results/
   ```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)\
Author: Vladislav Smolskii\
[LinkedIn](www.linkedin.com/in/vladislav-smolskii/)