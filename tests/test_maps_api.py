from utils.api import MapsAPI
from utils.checking import Checking
import allure

"""Create, read, update and delete new place"""


@allure.epic('Test CRUD new place')
class TestMapsAPI:

    @allure.feature('CRUD new place')
    def test_create_new_place(self):
        with allure.step('Create new place'):
            print('Method POST')
            result_post = MapsAPI.create_new_place()
            check_post = result_post.json()
            self.place_id = check_post.get('place_id')
            print(self.place_id)

            Checking.check_status_code(result_post, 200)
            Checking.check_json_token(result_post, ['status', 'place_id', 'scope', 'reference', 'id'])
            Checking.check_json_value(result_post, 'scope', 'APP')

        with allure.step('Get new place\'s info and check correctness'):
            print('Method GET after POST')
            result_get = MapsAPI.get_new_place(self.place_id)

            Checking.check_status_code(result_get, 200)
            Checking.check_json_token(result_get,
                                      ['location', 'accuracy', 'name', 'phone_number', 'address', 'types', 'website',
                                       'language'])
            Checking.check_json_value(result_get, 'address', '29, side layout, cohen 09')

        with allure.step('Update place\'s info'):
            print('Method PUT')
            print(self.place_id)
            result_put = MapsAPI.update_new_place(self.place_id)

            Checking.check_status_code(result_put, 200)
            Checking.check_json_token(result_put, ['msg'])
            Checking.check_json_value(result_put, 'msg', 'Address successfully updated')

        with allure.step('Get place\'s new info and check correctness'):
            print('Method GET after PUT')
            result_get = MapsAPI.get_new_place(self.place_id)
            print(f'New address: {result_get.json().get("address")}')

            Checking.check_status_code(result_get, 200)
            Checking.check_json_token(result_get,
                                      ['location', 'accuracy', 'name', 'phone_number', 'address', 'types', 'website',
                                       'language'])
            Checking.check_json_value(result_get, 'address', '100 Lenina street, RU')

        with allure.step('Delete the place'):
            print('Method DELETE')
            result_delete = MapsAPI.delete_new_place(self.place_id)

            Checking.check_status_code(result_delete, 200)
            Checking.check_json_token(result_delete, ['status'])
            Checking.check_json_value(result_delete, 'status', 'OK')

        with allure.step('Try to get deleted place\'s info'):
            print('Method GET after DELETE')
            result_get = MapsAPI.get_new_place(self.place_id)

            Checking.check_status_code(result_get, 404)
            Checking.check_json_token(result_get, ['msg'])
            Checking.check_json_value(result_get, 'msg', 'Get operation failed, looks like place_id  doesn\'t exists')
            Checking.check_json_value_by_word(result_get, 'msg', 'failed')
            Checking.check_json_value_by_word(result_get, 'msg', 'foiled')
            print('CRUD new location testing has done successful!')
