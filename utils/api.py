from utils.http_methods import HttpMethods

"""Methods for testing Maps API"""

base_url = 'https://rahulshettyacademy.com'
key = '?key=qaclick123'


class MapsAPI:

    @staticmethod
    def create_new_place():
        json_for_create_new_place = {
            "location": {
                "lat": -38.383494,
                "lng": 33.427362
            }, "accuracy": 50,
            "name": "Frontline house",
            "phone_number": "(+91) 983 893 3937",
            "address": "29, side layout, cohen 09",
            "types": [
                "shoe park",
                "shop"
            ],
            "website": "http://google.com",
            "language": "French-IN"
        }
        post_resource = '/maps/api/place/add/json'
        post_url = base_url + post_resource + key
        result_post = HttpMethods.post(post_url, body=json_for_create_new_place)
        return result_post

    @staticmethod
    def get_new_place(place_id):
        get_resource = '/maps/api/place/get/json'
        get_url = base_url + get_resource + key + '&place_id=' + place_id
        result_get = HttpMethods.get(get_url)
        return result_get

    @staticmethod
    def update_new_place(place_id):
        json_for_update_new_place = {
            "place_id": place_id,
            "address": "100 Lenina street, RU",
            "key": "qaclick123"
        }
        put_resource = '/maps/api/place/update/json'
        put_url = base_url + put_resource + key
        result_put = HttpMethods.put(put_url, body=json_for_update_new_place)
        return result_put

    @staticmethod
    def delete_new_place(place_id):
        json_for_delete_new_place = {
            "place_id": place_id
        }
        delete_resource = '/maps/api/place/delete/json'
        delete_url = base_url + delete_resource + key
        result_delete = HttpMethods.delete(delete_url, body=json_for_delete_new_place)
        return result_delete
