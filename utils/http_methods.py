import allure
import requests
from utils.logger import Logger


class HttpMethods:
    """HTTP methods list"""
    header = {'Content-Type': 'application/json'}
    cookie = ''

    @staticmethod
    def get(url):
        method_name = 'GET'
        with allure.step(method_name):
            Logger.add_request(url, method=method_name)
            result = requests.get(url, headers=HttpMethods.header, cookies=HttpMethods.cookie)
            Logger.add_response(result)
            return result

    @staticmethod
    def post(url, body):
        method_name = 'POST'
        with allure.step(method_name):
            Logger.add_request(url, method=method_name)
            result = requests.post(url, json=body, headers=HttpMethods.header, cookies=HttpMethods.cookie)
            Logger.add_response(result)
            return result

    @staticmethod
    def put(url, body):
        method_name = 'PUT'
        with allure.step(method_name):
            Logger.add_request(url, method=method_name)
            result = requests.put(url, json=body, headers=HttpMethods.header, cookies=HttpMethods.cookie)
            Logger.add_response(result)
            return result

    @staticmethod
    def delete(url, body):
        method_name = 'DELETE'
        with allure.step(method_name):
            Logger.add_request(url, method=method_name)
            result = requests.delete(url, json=body, headers=HttpMethods.header, cookies=HttpMethods.cookie)
            Logger.add_response(result)
            return result
