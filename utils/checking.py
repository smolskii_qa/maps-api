import json
from requests import Response


class Checking:
    """Methods for checking responses"""

    @staticmethod
    def check_status_code(response: Response, status_code):
        """Status-code checking"""
        assert status_code == response.status_code
        if response.status_code == status_code:
            print(f'Successful! Status-code {response.status_code}')
        else:
            print(f'Error! Status-code {response.status_code}')

    @staticmethod
    def check_json_token(response: Response, expected_value):
        """Expected response fields(keys) checking"""
        token = json.loads(response.text)
        assert list(token) == expected_value
        print('All expected response fields are included')

    @staticmethod
    def check_json_value(response: Response, field_name, expected_value):
        """Response value checking"""
        check = response.json()
        check_info = check.get(field_name)
        assert check_info == expected_value
        print(f'{field_name} correct!')

    @staticmethod
    def check_json_value_by_word(response: Response, field_name, expected_word):
        """Response value checking by word"""
        check = response.json()
        check_info = check.get(field_name)
        if expected_word in check_info:
            print(f'Field {field_name} includes {expected_word}')
        else:
            print(f'Word {expected_word} doesn\'t exist in {field_name}')
